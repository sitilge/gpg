# gpg

GPG key management.

## Setup

- `gpg --full-generate-key` - the default settings are the recommended ones.
- `gpg --edit-key martins@sitilge.id.lv` - subkey for encryption has been created in the initialization step, only add a subkey for signing.
- `gpg --export-secret-keys --armor martins@sitilge.id.lv > privkey.asc` - create the private key backup. Store in a safe place.
- `gpg --generate-revocation --armor martins@sitilge.id.lv > revoke.asc` - create the revocation certificate. Store in a safe place.
- `gpg --list-secret-keys --with-keygrip` - list all secret keys with their keygrips. Note that a keygrip is not an id but more like a file name of the key. Key id looks similar to `ssb rsa2048/0xA2C4A5CDABE2CAEA 2019-01-01 [E]`, where `A2C4A5CDABE2CAEA` is the key id.
- `gpg --export-secret-subkeys --armor [encryption subkey id]! > encryption.gpg` - export the encryption subkey ([E] in the list of keys).
- `gpg --export-secret-subkeys --armor [signing subkey id]! > signing.gpg` - export the signing subkey ([S] in the list of keys).
- `gpg --import encryption.gpg signing.gpg` - import the public subkey, the encryption subkey, and the signing subkey.
- `gpg --send-keys [key id]` - send the key to a keyserver.
- `gpg --delete-secret-keys [key id]` - delete the master key.
